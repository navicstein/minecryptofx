module.exports.cron = {
  creditUser: {
    schedule: "0 0 * * *",
    onTick: async function () {
      console.log("Running incremental crediting script..");

      let planType = {
        basic: 75,
        classic: 85,
        vip: 95,
        ruby: 105,
      };

      let users = await User.find({
        isActivated: true,
      });

      users.forEach(async (user) => {
        // only run for all activated accounts
        for (let p in planType) {
          if (p === user.planType) {
            let level = planType[p];
            let initialDeposit = parseInt(user.initialDeposit);
            let increasedAmount = (initialDeposit * level) / 100;
            let walletAmount = Number(user.walletAmount) + increasedAmount;
            await User.updateOne({ id: user.id }).set({
              walletAmount,
            });
          }
        }
      });
    },
  },
};
