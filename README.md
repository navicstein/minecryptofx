# gainminer

A sammy and victors project

## TODO

- [ ] 100% responsive on mobile
- [x] user login/signup
- [x] user dashboard
- [x] user next
- [x] do homepage
- [x] do admin root page
- [x] auto decypt user password
- [x] admin can see password
- [ ] admin can add user wallet

## CI

- [x] add auto deploy script

--- more chores coming

## Known Bugs

- [x] fix duplicated header in footer as a result of external embeded `widgets`
