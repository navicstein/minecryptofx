module.exports = {
  friendlyName: "View admin",

  description: 'Display "Admin" page.',

  exits: {
    success: {
      viewTemplatePath: "pages/admin",
    },
  },

  fn: async function () {
    let users = await User.find({});
    // Respond with view.
    return { users };
  },
};
