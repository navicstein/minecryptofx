module.exports = {
  friendlyName: "View user",

  description: 'Display "User" page.',

  inputs: {
    userId: {
      type: "string",
    },
  },
  exits: {
    success: {
      viewTemplatePath: "pages/admin/user",
    },
  },

  fn: async function ({ userId }) {
    const selectables = [
      "fullName",
      "password",
      "walletAmount",
      "accountType",
      "planType",
      "isActivated",
      "emailAddress",
      "initialDeposit",
      "bitcoinAddress",
      "isSuperAdmin",
    ];

    const user = await User.findOne({ id: userId }).select([...selectables]);

    // Respond with view.
    return { user, layout: "layouts/layout-admin" };
  },
};
