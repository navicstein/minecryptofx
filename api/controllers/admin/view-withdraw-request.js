module.exports = {
  friendlyName: "View withdraw request",

  description: 'Display "Withdraw request" page.',

  exits: {
    success: {
      viewTemplatePath: "pages/admin/withdraw-request",
    },
  },

  fn: async function () {
    let pendingWithdrawals = await Withdraw.find({})
      .sort("createdAt DESC")
      .populate("user");

    // Respond with view.
    return { pendingWithdrawals, layout: "layouts/layout-admin" };
  },
};
