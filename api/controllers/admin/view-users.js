// @ts-nocheck
module.exports = {
  friendlyName: "View users",

  description: 'Display "Users" page.',

  inputs: {
    activated: {
      type: "boolean",
    },
    unactivated: {
      type: "boolean",
    },

    userId: {
      type: "string",
    },
  },
  exits: {
    success: {
      viewTemplatePath: "pages/admin/users",
    },
  },

  fn: async function (inputs) {
    let { userId, activated } = inputs;

    let selectables = [
      "fullName",
      "password",
      "walletAmount",
      "accountType",
      "tosAcceptedByIp",
      "planType",
      "isActivated",
      "emailAddress",
      "initialDeposit",
      "bitcoinAddress",
    ];

    let args = {};

    if (activated) {
      args.isActivated = true;
    } else if (activated == false) {
      args.isActivated = false;
    }

    let users = await User.find(args).select([...selectables]);

    // Respond with view.
    return { users, layout: "layouts/layout-admin" };
  },
};
