module.exports = {
  friendlyName: "Update withdraw request",

  description: "",

  inputs: {
    wId: {
      type: "number",
      required: true,
    },
    status: {
      type: "string",
      required: false,
    },
  },

  exits: {},

  fn: async function ({ wId, status }) {
    let withdrawRequest = await Withdraw.updateOne({ id: wId }).set({ status });
    // All done.
    return withdrawRequest;
  },
};
