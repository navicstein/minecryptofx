module.exports = {


  friendlyName: 'View add money',


  description: 'Display "Add money" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/admin/add-money'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
