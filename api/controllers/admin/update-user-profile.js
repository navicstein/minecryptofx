module.exports = {
  friendlyName: "Update user profile",

  description: "",

  inputs: {
    id: {
      type: "string",
      required: true,
    },
    bitcoinAddress: {
      type: "string",
      required: false,
    },
    isActivated: {
      type: "boolean",
      defaultsTo: false,
    },
    planType: {
      type: "string",
      isIn: ["basic", "classic", "vip", "ruby"],
      required: false,
    },
    initialDeposit: {
      type: "string",
      required: false,
      defaultsTo: "0",
    },

    walletAmount: {
      type: "number",
      required: false,
    },
    fullName: {
      type: "string",
      required: true,
      description: "Full representation of the user's name.",
      example: "Mary Sue van der McHenst",
    },

    username: {
      type: "string",
      required: false,
    },

    password: {
      type: "string",
      required: true,
      description:
        "Securely hashed representation of the user's login password.",
      protect: true,
      example: "2$28a8eabna301089103-13948134nad",
    },

    emailAddress: {
      type: "string",
      required: true,
      unique: true,
      isEmail: true,
      maxLength: 200,
      example: "mary.sue@example.com",
    },

    accountType: {
      type: "string",
      required: false,
    },
  },

  exits: {},

  fn: async function (inputs) {
    let user = await User.updateOne({ id: inputs.id }).set({
      ...inputs,
    });
    // All done.
    return user;
  },
};
