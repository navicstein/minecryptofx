module.exports = {
  friendlyName: "View dashboard",

  description: 'Display "Dashboard" page.',

  exits: {
    success: {
      viewTemplatePath: "pages/admin/dashboard",
    },
  },

  fn: async function () {
    let activatedUsers = await User.find({
      isActivated: true,
      isSuperAdmin: false,
    });
    let unactivatedUsers = await User.find({
      isActivated: false,
      isSuperAdmin: false,
    });
    let userCount = await User.count({});
    //TODO: add withdrawal request

    let pendingWithdrawalsCount = await Withdraw.count({ isWithdrawn: false });
    let approvedWithdrawalsCount = await Withdraw.count({ isWithdrawn: false });

    // Respond with view.
    return {
      unactivatedUsers,
      activatedUsers,
      userCount,
      approvedWithdrawalsCount,
      pendingWithdrawalsCount,
      layout: "layouts/layout-admin",
    };
  },
};
