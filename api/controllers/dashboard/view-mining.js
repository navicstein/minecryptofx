module.exports = {
  friendlyName: "View mining",

  description: 'Display "Mining" page.',

  exits: {
    success: {
      viewTemplatePath: "pages/dashboard/mining",
    },
  },

  fn: async function () {
    let withdrawals;
    let admin = await User.findOne({ isSuperAdmin: true });

    if (this.req.me) {
      let myId = this.req.me.id;
      withdrawals = await Withdraw.count({ user: myId });
    }

    // Respond with view.
    return { withdrawals, admin, layout: "layouts/layout-user" };
  },
};
