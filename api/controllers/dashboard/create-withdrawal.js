module.exports = {
  friendlyName: "Create withdrawal",

  description: "creates a withdraw for the user to the admin",

  inputs: {
    withdrawAmount: { type: "string", required: true },
    description: { type: "string", required: false },
  },

  exits: {},

  fn: async function (inputs) {
    let withdrawal = await Withdraw.create({
      amount: inputs.withdrawAmount.trim(),
      isWithdrawn: false,
      user: this.req.me.id,
      description: inputs.description,
    });
    // All done.
    return withdrawal;
  },
};
