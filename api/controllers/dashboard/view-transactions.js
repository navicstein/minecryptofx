module.exports = {
  friendlyName: "View transactions",

  description: 'Display "Transactions" page.',

  exits: {
    success: {
      viewTemplatePath: "pages/dashboard/transactions",
    },
  },

  fn: async function () {
    let transactions = await Withdraw.find({
      user: this.req.me.id,
    })
      .sort("createdAt DESC")
      .populate("user");
    // Respond with view.
    return {
      transactions,
      layout: "layouts/layout-user",
    };
  },
};
