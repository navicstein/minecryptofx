jQuery
  .ajax({
    url: "https://min-api.cryptocompare.com/data/pricemulti",
    data: "fsyms=BTC,ETH,DASH,LTC&tsyms=USD",
    dataType: "json",
  })
  .done(function (data) {
    // console.log( "BTC : " + data.BTC.USD + ", ETH : " + data.ETH.USD + ", DASH : " + data.DASH.USD, LTC : " + data.LTC.USD);
    jQuery(".dashCoin").html("$" + data.DASH.USD);
    jQuery(".ethCoin").html("$" + data.ETH.USD);
    jQuery(".bitCoin").html("$" + data.BTC.USD);
    jQuery(".liteCoin").html("$" + data.LTC.USD);
  })
  .fail(function () {
    console.log("API error");
  });
