parasails.registerPage("homepage", {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    heroHeightSet: false,
    marqueeLoaded: false,
    benefits: [
      {
        title: " <h3>earn profit <span>daily</span></h3>",
        img: "/images/copy/homefeatured_icon1.png",
      },
      {
        title: "<h3>Minimum Deposit <span>as low as £550</span></h3>",
        img: "/images/copy/homefeatured_icon2.png",
      },
      {
        title: "<h3>earn up to <span>105% daily</span></h3>",
        img: "/images/copy/homefeatured_icon3.png",
      },
      {
        title: "<h3>withdraw <span>instantly</span></h3>",
        img: "/images/copy/homefeatured_icon4.png",
      },
      {
        title: "<h3> expert <span>traders</span></h3>",
        img: "/images/copy/homefeatured_icon5.png",
      },
      {
        title: "<h3>long term <span>program</span></h3>",
        img: "/images/copy/homefeatured_icon6.png",
      },
    ],
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function () {
    Vue.config.ignoredElements = ["coingecko-coin-price-marquee-widget"];
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);

    (function (b, i, t, C, O, I, N) {
      window.addEventListener(
        "load",
        function () {
          if (b.getElementById(C)) return;
          (I = b.createElement(i)), (N = b.getElementsByTagName(i)[0]);
          I.src = t;
          I.id = C;
          N.parentNode.insertBefore(I, N);
        },
        false
      );
    })(document, "script", "https://widgets.bitcoin.com/widget.js", "btcwdgt");
  },
  mounted: async function () {
    $(".owl-one").owlCarousel({
      loop: true,
      margin: 0,
      nav: false,
      responsiveClass: true,
      autoplay: false,
      autoplayTimeout: 5000,
      autoplaySpeed: 1000,
      autoplayHoverPause: false,
      responsive: {
        0: {
          items: 1,
          nav: false,
        },
        480: {
          items: 1,
          nav: false,
        },
        667: {
          items: 1,
          nav: true,
        },
        1000: {
          items: 1,
          nav: true,
        },
      },
    });

    this._setHeroHeight();
    // set the widgets
    window.onload = () => {
      let TVW = new TradingView.widget({
        autosize: true,
        symbol: "FX:EURUSD",
        interval: "1",
        timezone: "Etc/UTC",
        theme: "Light",
        style: "1",
        locale: "en",
        toolbar_bg: "#f1f3f6",
        enable_publishing: false,
        allow_symbol_change: true,
        container_id: "tradingview_12619",
      });
      this.marqueeLoaded = !!TVW;

      // hero desc
      // new TradingView.widget({
      //   width: 432,
      //   height: 240,
      //   autosize: !true,
      //   symbol: "COINBASE:BTCUSD",
      //   interval: "D",
      //   timezone: "Etc/UTC",
      //   theme: "Dark",
      //   style: "0",
      //   locale: "en",
      //   toolbar_bg: "#f1f3f6",
      //   enable_publishing: false,
      //   allow_symbol_change: true,
      //   hotlist: true,
      //   container_id: "tradingview_19631",
      // });

      console.log("mounted trading  view!");
    };
  },
  updated() {},
  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    clickHeroButton: async function () {
      // Scroll to the 'get started' section:
      $("html, body").animate(
        {
          scrollTop: this.$find('[role="scroll-destination"]').offset().top,
        },
        500
      );
    },

    // Private methods not tied to a particular DOM event are prefixed with _
    _setHeroHeight: function () {
      var $hero = this.$find("[full-page-hero]");
      var headerHeight = $("#page-header").outerHeight();
      var heightToSet = $(window).height();
      heightToSet = Math.max(heightToSet, 500); //« ensure min height of 500px - header height
      heightToSet = Math.min(heightToSet, 1000); //« ensure max height of 1000px - header height
      $hero.css("min-height", heightToSet - headerHeight + "px");
      this.heroHeightSet = true;
    },
  },
});
