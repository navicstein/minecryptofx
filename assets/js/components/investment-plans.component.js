parasails.registerComponent("investment-plans", {
  template: `
    <div class="plan_wrap">
      <div class="row">
        <h1 class="slideInUp wow">Choose Your Investment Plan</h1>
        <div class="planbox">
          <div class="plan flipInY wow">
            <h2>BASIC</h2>
            <h3 class="planone">75%</h3>
            <ul>
              <li>Daily for 5 days</li>
              <li>$550~$5000</li>
              <li>Instant Withdraw</li>
              <li>Referral Commission: 10%</li>
              <li>We only accept Bitcoin</li>
              <li>Principal Included</li>
            </ul>
            <a href="/signup/?planType=basic">Sign Up</a>
          </div>
          <div class="plan flipInY wow">
            <h2>CLASSIC</h2>
            <h3 class="plantwo">85%</h3>
            <ul>
              <li>Daily for 7 days</li>
              <li>$9500~$25000</li>
              <li>Instant Withdraw</li>
              <li>Referral Commission: 15%</li>
              <li>We only accept Bitcoin</li>
              <li>Principal Included</li>
            </ul>
            <a href="/signup/?planType=classic">Sign Up</a>
          </div>
          <div class="plan flipInY wow">
            <h2>VIP</h2>
            <h3 class="planthree">95%</h3>
            <ul>
              <li>Daily for 14 days</li>
              <li>$10000~$40000</li>
              <li>Instant Withdraw</li>
              <li>Referral Commission: 20%</li>
              <li>We only accept Bitcoin</li>
              <li>Principal Included</li>
            </ul>
            <a href="/signup/?planType=vip">Sign Up</a>
          </div>
          <div class="plan flipInY wow">
            <h2>RUBY</h2>
            <h3 class="planfour">105%</h3>
            <ul>
              <li>Daily for 21 days</li>
              <li>$25000~80000</li>
              <li>Instant Withdraw</li>
              <li>Referral Commission: 25%</li>
              <li>We only accept Bitcoin</li>
              <li>Principal Included</li>
            </ul>
            <a href="/signup/?planType=ruby">Sign Up</a>
          </div>
        </div>
      </div>
    </div>
`,
});
