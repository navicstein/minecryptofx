parasails.registerComponent("siteFooter", {
  template: `<footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="copyright"><a href="?a=home"><img src="styles/images/footer_logo.png" alt=""></a></div>
          <p>Copyright &copy; 2019 gainminer.com All rights reserved.</p>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <h3>Useful Links</h3>
          <ul>
            <li><a href="?a=home">Home</a></li>
            <li><a href="?a=cust&page=about">About Us</a></li>
            <li><a href="?a=news">News</a></li>
            <li><a href="?a=faq">FAQ</a></li>
          </ul>
          <ul>
            <li><a href="?a=cust&page=banners">Banners</a></li>
            <li><a href="?a=cust&page=howto">How to Invest</a></li>
            <li><a href="?a=support">Support</a></li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <h3>Location</h3>
          <p>Unit 1 Lancaster Court, Coronation Road, Cressex Business Park, High Wycombe, United Kingdom</p>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <h3>Contacts</h3>
          <p>admin@gainminer.com<br>
            -<br>
           -</p>
            
           <h2>Phone</h2>
              <h2>USA AGENT</h2>
              <p>+15733423311</p> 
            
        </div>
      </div>
    </div>
  </footer>`,
});
